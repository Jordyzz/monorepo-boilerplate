import React from "react";
import Layout from "../components/Layout";

const checkEmail = () => {
  return (
    <Layout title="check email">
      Check your email to confirm your account!
    </Layout>
  );
};

export default checkEmail;
